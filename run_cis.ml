let out_file    : string option ref = ref None
let basis_file  : string option ref = ref None
let nuclei_file : string option ref = ref None
let charge      : int ref = ref 0
let multiplicity: int ref = ref 1


let speclist = [
  ( "-b" , Arg.String (fun x -> basis_file := Some x),
    "File containing the atomic basis set") ;
  ( "-c" , Arg.String (fun x -> nuclei_file := Some x),
    "File containing the nuclear coordinates") ;
  ( "-o" , Arg.String (fun x -> out_file := Some x) ,
    "Output file") ;
  ( "-charge" , Arg.Int (fun x -> charge := x),
    "Charge of the system") ;
  ( "-mult" , Arg.Int (fun x -> multiplicity := x),
    "Spin multiplicity of the system") ;
]

let run ~out =
  (*
  let gc = Gc.get () in
  Gc.set { gc with minor_heap_size=(262144 / 16) };
  *)
  let basis_file = 
    match !basis_file with
    | None -> raise (Invalid_argument "Basis set file should be specified with -b")
    | Some x -> x
  and nuclei_file = 
    match !nuclei_file with
    | None -> raise (Invalid_argument "Coordinate file should be specified with -c")
    | Some x -> x
  and charge = !charge
  and multiplicity = !multiplicity
  in

  let s =
    Simulation.of_filenames ~charge ~multiplicity ~nuclei:nuclei_file basis_file 
  in

  let hf = HartreeFock.make s in
  let mos = 
    MOBasis.of_hartree_fock hf 
  in
  let ee_ints = MOBasis.ee_ints mos in
  ERI.to_file ~filename:("mo.eri") ee_ints

let () =
  let usage_msg = "Available options:" in
  Arg.parse speclist (fun _ -> ()) usage_msg;
  run ~out:!out_file


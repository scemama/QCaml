type t =
| Pos 
| Neg

val of_nperm : int -> t
(** Returns the phase obtained by a given number of permuations. *)

val to_nperm : t -> int
(** Converts the phase to [1] or [0] permutations. *)

val add : t -> t -> t
(** Add a given phase to an existing phase. *)

val add_nperm : t -> int -> t
(** Add to an existing phase a given number of permutations. *)

val neg : t -> t
(** Negate the phase. *)

(** {1 Printers} *)

val pp : Format.formatter -> t -> unit

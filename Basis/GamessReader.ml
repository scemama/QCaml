(** Reads a basis set in GAMESS format *)
let read_basis filename =
  let lexbuf =
    let ic = open_in filename in
    Lexing.from_channel ic
  in
  let rec aux accu = 
    try
      let key, basis =
        GamessParser.input BasisLexer.read_all lexbuf
      in
      aux ((key, basis)::accu)
    with
    | Parsing.Parse_error -> List.rev accu
  in
  aux []


(** All constants used in the program.
 *)

(** {2 Thresholds } *)

val epsilon : float
(** Value below which a float is considered null. Default is {% $\epsilon$ %} = 10{^-20}. *)

val integrals_cutoff : float
(** Cutoff value for integrals. Default is 10{^-15}. *)

(** {2 Mathematical constants } *)

val pi : float
(** {% $\pi$ %} = 3.141_592_653_589_793_12 *)

val two_pi : float
(** {% $2\pi$ %} *)

val sq_pi : float
(** {% $\sqrt{\pi}$ %} *)

val sq_pi_over_two : float
(** {% $\frac{\sqrt{\pi}}{2}$ %} *)

val pi_inv : float
(** {% $\frac{1}{\pi}$ %} *)

val two_over_sq_pi : float
(** {% $\frac{2}{\sqrt{\pi}}$ %} *)

(** {2 Physical constants} *)

val a0 : float
(** Bohr radius : {% $a_0$ %} = 0.529 177 210 67(23) Angstrom *)

val a0_inv : float
(** {% $\frac{1}{a_0}$ %} *)

val ha_to_ev : float
(** Hartree to eV conversion factor :: 27.211 386 02(17) *)

val ev_to_ha : float
(** eV to Hartree conversion factor : 1/{ha_to_ev} *)


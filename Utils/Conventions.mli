(** Contains the conventions relative to the program.

  The phase convention is given by:
  {% $\sum_i c_i > 0$ %} or {% $\min_i c_i \ge  0$ %}

*)

open Lacaml.D


val in_phase : Vec.t -> bool
(** Checks if one MO respects the phase convention *)

val rephase : Mat.t -> Mat.t
(** Apply the phase convention to the MOs. *)


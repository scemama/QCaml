(** A range is a sorted list of integers in an interval.
 
  {[ "[36-53,72-107,126-131]" ]}
  represents the list of integers
  {[ [ 37 ; 37 ; 38 ; ... ; 52 ; 53 ; 72 ; 73 ; ... ; 106 ; 107 ; 126 ; 127 ; ...
  ; 130 ; 131 ] ]}
*)

type t

val of_string : string -> t
(** Create from a string:

    - "[a-b]" : range between a and b (included)
    - "[a]"   : the list with only one integer a
    - "a"     : equivalent to "[a]"
*)


val to_string : t -> string
(** String representation. *)


val to_int_list : t -> int list
(** Transform into a list of ints. *)

(** {2 Printers} *)

val pp : Format.formatter -> t -> unit


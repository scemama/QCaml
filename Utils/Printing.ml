let line ?(c='-') n =
  String.make n c

let ppf_dev_null = 
  let oc = open_out "/dev/null" in
  Format.formatter_of_out_channel oc


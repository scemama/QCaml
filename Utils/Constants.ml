let integrals_cutoff  = 1.e-15
let epsilon = 1.e-20

(** Constants *)
let pi = acos (-1.)
let two_pi = 2. *. pi
let sq_pi = sqrt pi
let sq_pi_over_two = sq_pi *. 0.5
let pi_inv = 1. /. pi
let two_over_sq_pi = 2. /. (sqrt pi)

let a0 = 0.529_177_210_67
let a0_inv = 1. /. a0
let ha_to_ev = 27.211_386_02
let ev_to_ha = 1. /. ha_to_ev

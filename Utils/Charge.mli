(** This type should be used for all charges in the program (electrons, nuclei,...). *)

type t 

(** {2 Conversions} *)

val of_float : float -> t
val of_int   : int -> t
val of_string: string -> t

val to_float : t -> float
val to_int   : t -> int
val to_string: t -> string 

(** {2 Operations} *)

val ( + ) : t -> t -> t
val ( - ) : t -> t -> t
val ( * ) : t -> float -> t
val ( / ) : t -> float -> t 

(** {2 Printers } *)

val pp : Format.formatter -> t -> unit


type t = { x: int ; y : int ; z : int ; tot : int }

let of_int_tuple t = 
  let result =
    match t with 
    | (x,y,z) -> { x ; y ; z ; tot=x+y+z }
  in
  if result.x < 0 ||
     result.y < 0 ||
     result.z < 0 ||
     result.tot < 0 then
      invalid_arg (__FILE__^": of_int_tuple");
  result

let to_int_tuple { x ; y ; z ; _ } = (x,y,z) 

let get coord t =
  match coord with
  | Coordinate.X -> t.x
  | Coordinate.Y -> t.y
  | Coordinate.Z -> t.z

let incr coord t =
  match coord with
  | Coordinate.X -> let r = t.x+1 in { t with x = r ; tot = t.tot+1 }
  | Coordinate.Y -> let r = t.y+1 in { t with y = r ; tot = t.tot+1 }
  | Coordinate.Z -> let r = t.z+1 in { t with z = r ; tot = t.tot+1 }

let decr coord t =
  match coord with
  | Coordinate.X -> let r = t.x-1 in { t with x = r ; tot = t.tot-1 }
  | Coordinate.Y -> let r = t.y-1 in { t with y = r ; tot = t.tot-1 }
  | Coordinate.Z -> let r = t.z-1 in { t with z = r ; tot = t.tot-1 }




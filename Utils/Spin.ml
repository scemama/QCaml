(** Electron spin *)

type t =  (* m_s *)
| Alfa (* {% $m_s = +1/2$ %} *)
| Beta (* {% $m_s = -1/2$ %} *)

let other = function
| Alfa -> Beta
| Beta -> Alfa

(*
let half = 1. /. 2.


(** {% $\alpha(m_s)$ %} *)
let alfa = function 
| n, Alfa -> n
| _, Beta -> 0.


(** {% $\beta(m_s)$ %} *)
let beta = function 
| _, Alfa -> 0.
| n, Beta -> n


(** {% $S_z(m_s)$ %} *)
let s_z = function
| n, Alfa ->    half *. n, Alfa
| n, Beta -> -. half *. n, Beta

let s_plus = function
| n, Beta -> n , Alfa
| _, Alfa -> 0., Alfa

let s_minus = function
| n, Alfa -> n , Beta
| _, Beta -> 0., Beta

let ( ++ ) (n, t) (m,t) =
  (m. +n.)

let s2 s =
  s_minus @@ s_plus s +.
  s_z s +.
  s_z @@ s_z s
  *)

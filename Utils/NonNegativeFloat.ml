type t = float 

let of_float x =
  if x < 0. then invalid_arg (__FILE__^": of_float");
  x

external to_float : t -> float = "%identity"
external unsafe_of_float : float -> t = "%identity"

let to_string x =
  let f = to_float x in string_of_float f

let of_string x =
 let f = float_of_string x in of_float f

